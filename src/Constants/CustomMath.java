package Constants;

import Objects.Coordinate;

/**
 * Created by L. Keijzer on 06-Nov-16.
 */
public final class CustomMath {

    public static double dist(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }

    public static double dist(Coordinate c1, Coordinate c2) {
        return dist(c1.getX(), c1.getY(), c2.getX(), c2.getY());
    }

    /**
     * area triangle = (Ax(By - Cy) + Bx(Cy - Ay) + Cx(Ay - By)) / 2
     * */
    public static double calcAreaTriangle(final Coordinate A, final Coordinate B, final Coordinate C) {
        return Math.abs((
                A.getX() * (B.getY() - C.getY()) +
                        B.getX() * (C.getY() - A.getY()) +
                        C.getX() * (A.getY() - B.getY())) / 2);
    }

    private CustomMath() {
    }
}
