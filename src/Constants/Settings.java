package Constants;

/**
 * Created by L. Keijzer on 27-Oct-16.
 */
public final class Settings {
    // TODO: remove class

    // Paddle
    public static final float PADDLE_MOVE_SPEED = 0.5f;  // X pixel per s
    public static final int PADDLE_WIDTH = 100;
    public static final int PADDLE_HEIGHT = 20;
    public static final int PADDLE_START_OFFSET = 100;
    public static final int PADDLE_MAX_TOTAL_ANGLE_ADJUST = 90; // divide by 2 to get the max angle per side

    // Ball
    public static final float BALL_START_MOVE_SPEED = 0.5f;  // X pixel per s
    public static final int BALL_DIAMETER = 20;
    public static final int AMOUNT_OF_BALLS = 1;
    public static final float BALL_MAX_SPEED = 1f;
    public static final float BALL_INCREASE_SPEED = 0.02f;

    // Window
    public static int SCREEN_WIDTH = 1500;
    public static int SCREEN_HEIGHT = 1000;
    public static final int TARGET_FPS = 60;      // needs to be lower than 500

    // Keys
    public static final int KEY_P1_UP = 87;
    public static final int KEY_P1_DOWN = 83;
    public static final int KEY_P2_UP = 38;
    public static final int KEY_P2_DOWN = 40;

    // Game
    public static final int TIME_DELAwY_BALL_MOVING = 100; // time in ms before the ball starts to move
    public static int AMOUNT_PLAYERS = 2;

    private Settings() {
    }
}
