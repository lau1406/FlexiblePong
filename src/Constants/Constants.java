package Constants;

import java.awt.*;

/**
 * Created by L. Keijzer on 30-Oct-16.
 */
public final class Constants {
    // Constants
        // Screen
    private static final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    public static final double SCREEN_WIDTH = screenSize.getWidth();
    public static final double SCREEN_HEIGHT = screenSize.getHeight();

        // General
    public static final float SMALL_NUMBER = 0.01f;   // Floating point error
    public static final float VERY_SMALL_NUMBER = 0.00001f;   // Floating point error

        // Window
    public static final int SCORE_HEIGHT_OFFSET = 50;
    public static final String TITLE = "Pong";
    public static final String TITLE_MAIN_MENU = TITLE + " Main Menu";
    public static final String TITLE_SETTINGS = TITLE + " Settings";
    public static final int WINDOW_ITEMS_HIEGHT = 50;

        // Text on stuff
    public static final String BUTTON_LOCAL_NAME = "Local";
    public static final String BUTTON_INTERWEBS_NAME = "Online";
    public static final String BUTTON_SETTINGS_NAME = "Settings";
    public static final String BUTTON_EXIT_NAME = "Exit";

        // Names
    public static final String PLAYER_KEYBOARD_NAME = "Keyboard Player";


    // Default values
        // Paddle
    public static final float DEFAULT_PADDLE_MOVE_SPEED = 0.5f;  // X pixel per s
    // TODO: convert width and height to percentage instead of fixed values
    public static final int DEFAULT_PADDLE_WIDTH = 10;
    public static final int DEFAULT_PADDLE_HEIGHT = 20;
    public static final int DEFAULT_PADDLE_START_OFFSET = 100;
    public static final int DEFAULT_PADDLE_MAX_TOTAL_ANGLE_ADJUST = 90; // divide by 2 to get the max angle per side

        // Ball
    public static final float DEFAULT_BALL_START_MOVE_SPEED = 0.5f;  // X pixel per s
    public static final int DEFAULT_BALL_DIAMETER = 20;
    public static final int DEFAULT_AMOUNT_OF_BALLS = 1;
    public static final float DEFAULT_BALL_MAX_MOVE_SPEED = 1f;
    public static final float DEFAULT_BALL_INCREASE_MOVE_SPEED = 0.02f;

        // Window
    public static final int DEFAULT_SCREEN_WIDTH = 1500;
    public static final int DEFAULT_SCREEN_HEIGHT = 1000;
    public static final int DEFAULT_TARGET_FPS = 60;

        // Game
    public static final int DEFAULT_TIME_DELAY_BALL_MOVING = 1; // time in ms before the ball starts to move
    public static final int DEFAULT_AMOUNT_PLAYERS = 6;


    // Preferences string names
        // Paddle
    public static final String PREF_PADDLE_MOVE_SPEED = "paddleMoveSpeed";
    public static final String PREF_PADDLE_WIDTH = "paddleWidth";
    public static final String PREF_PADDLE_HEIGHT = "paddleHeight";
    public static final String PREF_PADDLE_START_OFFSET = "paddleStartOffset";
    public static final String PREF_PADDLE_MAX_TOTAL_ANGLE_ADJUST = "paddleMaxTotalAngleAdjust";
        // Ball
    public static final String PREF_BALL_START_MOVE_SPEED = "ballStartMoveSpeed";
    public static final String PREF_BALL_DIAMETER = "ballDiameter";
    public static final String PREF_AMOUNT_OF_BALLS = "amountOfBalls";
    public static final String PREF_BALL_MAX_SPEED = "ballMaxSpeed";
    public static final String PREF_BALL_INCREASE_SPEED = "ballIncreaseSpeed";
        // Window
    public static final String PREF_SCREEN_WIDTH = "screenWidth";
    public static final String PREF_SCREEN_HEIGHT = "screenHeight";
    public static final String PREF_TARGET_FPS = "targetFps";
        // Game
    public static final String PREF_TIME_DELAY_BALL_MOVING = "timeDelayBallMoving";
    public static final String PREF_AMOUNT_PLAYERS = "amountPlayers";

    // Label text
        // Paddle
    public static final String TEXT_PADDLE_MOVE_SPEED = "Paddle Move Speed";
    public static final String TEXT_PADDLE_WIDTH = "Paddle Width";
    public static final String TEXT_PADDLE_HEIGHT = "Paddle Height";
    public static final String TEXT_PADDLE_START_OFFSET = "Paddle Start Offset";
    public static final String TEXT_PADDLE_MAX_TOTAL_ANGLE_ADJUST = "Paddle Angle Adjust";
        // Ball
    public static final String TEXT_BALL_START_MOVE_SPEED = "Ball Start Move Speed";
    public static final String TEXT_BALL_DIAMETER = "Ball Size";
    public static final String TEXT_AMOUNT_OF_BALLS = "Amount of Balls";
    public static final String TEXT_BALL_MAX_SPEED = "Maximum speed of the ball";
    public static final String TEXT_BALL_INCREASE_SPEED = "Ball Increase Speed";
        // Window
    public static final String TEXT_SCREEN_WIDTH = "Screen Width";
    public static final String TEXT_SCREEN_HEIGHT = "Screen Height";
    public static final String TEXT_TARGET_FPS = "FPS";
        // Game
    public static final String TEXT_TIME_DELAY_BALL_MOVING = "Time Delay Before Moving";
    public static final String TEXT_AMOUNT_PLAYERS = "Amount of PLayers";
        // Buttons
    public static final String TEXT_BUTTON_ADD_PLAYER = "Add Player";
    public static final String TEXT_BUTTON_RESET = "Reset";
    public static final String TEXT_BUTTON_EXIT = "Exit";

    private Constants() {
    }
}
