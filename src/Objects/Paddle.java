package Objects;

import Constants.CustomMath;

import java.awt.*;

import static Constants.Constants.VERY_SMALL_NUMBER;
import static Constants.Settings.*;
import static Gui.Settings.*;

/**
 * Created by L. Keijzer on 27-Oct-16.
 */
public class Paddle {
    /**
     * Drawing of a paddle
     * [] = corner
     * -> = the direction the angle points
     * <p>
     * |-------width--------|   _
     * [LT]--------------[RT]   |
     * |        <---        |   height
     * [LB]--------------[RB]   |
     */

    private Coordinate mCornerLeftTop, mCornerLeftBottom, mCornerRightTop, mCornerRightBottom;
    // 1 and 4 are on the angle, and so are 2 and 3
    private float mAngle;
    private final int PADDLE_NUMBER;
    private long mStartMoveTime;        // Last moment in time the paddle was moved. This to make moving objects
    // frame-rate independent


    // Only 2 coordinates are needed to get all 4
    public Paddle(Coordinate cornerLeftTop, Coordinate cornerLeftBottom, Coordinate cornerRightTop, Coordinate
            cornerRightBottom, float angle, int number) {
        mCornerLeftTop = cornerLeftTop;
        mCornerLeftBottom = cornerLeftBottom;
        mCornerRightTop = cornerRightTop;
        mCornerRightBottom = cornerRightBottom;
        mAngle = angle;
        PADDLE_NUMBER = number;
        mStartMoveTime = System.currentTimeMillis();
    }

    public void move(float xAmount, float yAmount, Polygon polygon) {
        move(xAmount, yAmount);
//        mCornerLeftTop.setX(mCornerLeftTop.getX() + xAmount);
//        mCornerLeftTop.setY(mCornerLeftTop.getY() + yAmount);
//
//        mCornerLeftBottom.setX(mCornerLeftBottom.getX() + xAmount);
//        mCornerLeftBottom.setY(mCornerLeftBottom.getY() + yAmount);
//
//        mCornerRightTop.setX(mCornerRightTop.getX() + xAmount);
//        mCornerRightTop.setY(mCornerRightTop.getY() + yAmount);
//
//        mCornerRightBottom.setX(mCornerRightBottom.getX() + xAmount);
//        mCornerRightBottom.setY(mCornerRightBottom.getY() + yAmount);

        // TODO: change collision detection with walls to take into account a polygon, now only a rectangle
        if (AMOUNT_PLAYERS == 1) {
            // TODO: single player
        } else if (AMOUNT_PLAYERS == 2) {
            // Begin index
            checkCoordinateOnLine(polygon, mCornerLeftBottom, PADDLE_NUMBER * 2);
            checkCoordinateOnLine(polygon, mCornerRightBottom, PADDLE_NUMBER * 2);
        } else {
            checkCoordinateOnLine(polygon, mCornerLeftBottom, PADDLE_NUMBER);
            checkCoordinateOnLine(polygon, mCornerRightBottom, PADDLE_NUMBER);
        }

//        if (CustomMath.dist(
//                polygon.xpoints[PADDLE_NUMBER],
//                polygon.ypoints[PADDLE_NUMBER],
//                polygon.xpoints[(PADDLE_NUMBER + 1) % AMOUNT_PLAYERS],
//                polygon.ypoints[(PADDLE_NUMBER + 1) % AMOUNT_PLAYERS]) < CustomMath.dist(
//                        mCornerLeftBottom, new Coordinate(polygon.xpoints[PADDLE_NUMBER], polygon.ypoints[PADDLE_NUMBER])
//        ))


        // Dif with the top of the screen
        // The top of the field is at X=0

        float topDif = Math.min(
                Math.min(
                        mCornerLeftTop.getY(),
                        mCornerLeftBottom.getY()),
                Math.min(
                        mCornerRightTop.getY(),
                        mCornerRightBottom.getY()));

        // Dif with the bottom of the screen
        // The bottom of the field is at SCREEN_HEIGHT
        float bottomDif = Math.min(
                Math.min(
                        loadScreenHeight() - mCornerLeftTop.getY(),
                        loadScreenHeight() - mCornerLeftBottom.getY()),
                Math.min(
                        loadScreenHeight() - mCornerRightTop.getY(),
                        loadScreenHeight() - mCornerRightBottom.getY()));

        // Dif with the left side of the screen
        // The left side of the field is at Y=0
        float leftDif = Math.min(
                Math.min(
                        mCornerLeftTop.getX(),
                        mCornerLeftBottom.getX()),
                Math.min(
                        mCornerRightTop.getX(),
                        mCornerRightBottom.getX()
                ));

        // Dif with the right side of the screen
        // The right side of the field is at Y=SCREEN_WIDTH
        float rightDif = Math.min(
                Math.min(
                        loadScreenWidth() - mCornerLeftTop.getX(),
                        loadScreenWidth() - mCornerLeftBottom.getX()),
                Math.min(
                        loadScreenWidth() - mCornerRightTop.getX(),
                        loadScreenWidth() - mCornerRightBottom.getX()
                ));

        if (topDif < 0) {
            // One or more of the points are above the screen
            mCornerLeftTop.setY(mCornerLeftTop.getY() - topDif);
            mCornerLeftBottom.setY(mCornerLeftBottom.getY() - topDif);
            mCornerRightTop.setY(mCornerRightTop.getY() - topDif);
            mCornerRightBottom.setY(mCornerRightBottom.getY() - topDif);
        }

        if (bottomDif < 0) {
            // One or more of the points are below the screen
            mCornerLeftTop.setY(mCornerLeftTop.getY() + bottomDif);
            mCornerLeftBottom.setY(mCornerLeftBottom.getY() + bottomDif);
            mCornerRightTop.setY(mCornerRightTop.getY() + bottomDif);
            mCornerRightBottom.setY(mCornerRightBottom.getY() + bottomDif);
        }

        if (leftDif < 0) {
            // One or more of the points are above the screen
            mCornerLeftTop.setX(mCornerLeftTop.getX() - leftDif);
            mCornerLeftBottom.setX(mCornerLeftBottom.getX() - leftDif);
            mCornerRightTop.setX(mCornerRightTop.getX() - leftDif);
            mCornerRightBottom.setX(mCornerRightBottom.getX() - leftDif);
        }

        if (rightDif < 0) {
            // One or more of the points are above the screen
            mCornerLeftTop.setX(mCornerLeftTop.getX() + rightDif);
            mCornerLeftBottom.setX(mCornerLeftBottom.getX() + rightDif);
            mCornerRightTop.setX(mCornerRightTop.getX() + rightDif);
            mCornerRightBottom.setX(mCornerRightBottom.getX() + rightDif);
        }

        setStartMoveTime(System.currentTimeMillis());
    }

    private void checkCoordinateOnLine(Polygon polygon, Coordinate coordinate, int index) {
        float distance1 = (float) CustomMath.dist(new Coordinate(polygon.xpoints[index],
                polygon.ypoints[index]), coordinate);
        // Distance2 is the distance to the right most point
        float distance2 = (float) CustomMath.dist(new Coordinate(polygon.xpoints[(index + 1) % polygon.npoints],
                polygon.ypoints[(index + 1) % polygon.npoints]), coordinate);
        float totalDistance = (float) CustomMath.dist(polygon.xpoints[index], polygon.ypoints[index],
                polygon.xpoints[(index + 1) % polygon.npoints],
                polygon.ypoints[(index + 1) % polygon.npoints]);
        // I don't like floating point errors, they mess with math
        if (totalDistance < VERY_SMALL_NUMBER) totalDistance = 0f;
        if (distance1 < VERY_SMALL_NUMBER) distance1 = 0f;
        if (distance2 < VERY_SMALL_NUMBER) distance2 = 0f;

        // Distance of 0.5 isn't rly noticeable for error correction
        if (distance1 + distance2 - totalDistance > 0.5) {
            // Coordinate is not on the line
            // Calc distance and move points back accordingly
            move(distance1 > distance2, Math.min(distance1, distance2));
        }
    }

    private void move(boolean left, float distance) {
        float angle;
        if (left) {
            angle = mAngle;
        } else {
            angle = (mAngle + 180) % 360;
        }

        float xDif = (float) Math.sin(Math.toRadians(angle)) * distance;
        float yDif = (float) Math.cos(Math.toRadians(angle)) * distance * -1;
        if (Math.abs(xDif) < VERY_SMALL_NUMBER) {
            xDif = 0f;
        }
        if (Math.abs(yDif) < VERY_SMALL_NUMBER) {
            yDif = 0f;
        }

        move(xDif, yDif);
    }

    private void move(float xDif, float yDif) {
        // Calculate and set new coordinates
        mCornerLeftTop.setX(mCornerLeftTop.getX() + xDif);
        mCornerLeftTop.setY(mCornerLeftTop.getY() + yDif);

        mCornerLeftBottom.setX(mCornerLeftBottom.getX() + xDif);
        mCornerLeftBottom.setY(mCornerLeftBottom.getY() + yDif);

        mCornerRightTop.setX(mCornerRightTop.getX() + xDif);
        mCornerRightTop.setY(mCornerRightTop.getY() + yDif);

        mCornerRightBottom.setX(mCornerRightBottom.getX() + xDif);
        mCornerRightBottom.setY(mCornerRightBottom.getY() + yDif);
    }

    public float getAngleMain() {
        // TODO: check math
//        return (int) Math.toDegrees(Math.atan((mCornerLeftTop.getX() - mCornerRightBottom.getX()) / (mCornerLeftTop.getY() - mCornerRightBottom.getY())));
//        return (int) Math.toDegrees(Math.atan2((mCornerLeftTop.getX() - mCornerRightBottom.getX()) / (mCornerLeftTop
//                .getY() - mCornerRightBottom.getY())));
        return mAngle;
    }

    public double getArea() {
        return getWidth() * getHeight();
    }

    public float getWidth() {
        return (float) CustomMath.dist(mCornerLeftTop, mCornerRightTop);
    }

    public float getHeight() {
        return (float) CustomMath.dist(mCornerLeftTop, mCornerLeftBottom);
    }

    public Coordinate getCornerLeftTop() {
        return mCornerLeftTop;
    }

    public void setCornerLeftTop(Coordinate cornerLeftTop) {
        mCornerLeftTop = cornerLeftTop;
    }

    public Coordinate getCornerLeftBottom() {
        return mCornerLeftBottom;
    }

    public void setCornerLeftBottom(Coordinate cornerLeftBottom) {
        mCornerLeftBottom = cornerLeftBottom;
    }

    public Coordinate getCornerRightTop() {
        return mCornerRightTop;
    }

    public void setCornerRightTop(Coordinate cornerRightTop) {
        mCornerRightTop = cornerRightTop;
    }

    public Coordinate getCornerRightBottom() {
        return mCornerRightBottom;
    }

    public void setCornerRightBottom(Coordinate cornerRightBottom) {
        mCornerRightBottom = cornerRightBottom;
    }

    public long getStartMoveTime() {
        return mStartMoveTime;
    }

    public void setStartMoveTime(long startMoveTime) {
        mStartMoveTime = startMoveTime;
    }
}
