package Objects;

/**
 * Created by L. Keijzer on 29-Oct-16.
 */
public class Coordinate {
    private float x;
    private float y;

    public Coordinate(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void print() {
        System.out.println("coordinate x:y = " + x + ":" + y);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
