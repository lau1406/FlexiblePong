package Gui;

import Balls.AbstractBall;
import Objects.Paddle;
import Players.AbstractPlayer;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

import static Constants.Constants.SCORE_HEIGHT_OFFSET;
import static Gui.Settings.*;

/**
 * Created by L. Keijzer on 27-Oct-16.
 */
public class Field extends JPanel {
//    private Paddle mPaddleLeft, mPaddleRight;
//    private ArrayList<Paddle> mPaddles;
    private ArrayList<AbstractBall> mBalls;
    private ArrayList<AbstractPlayer> mPlayers;
    private Polygon mPolygon;
    private int mGamestate; // 0: before game(ball is nog moving), 1: in game(ball is moving), 2: after game(someone won)
    private long mTimeRemaining;

    public Field(Dimension dimension, ArrayList<AbstractBall> balls, ArrayList<AbstractPlayer> players, Polygon polygon,
                 int gameState, long timeRemaining) {
        setPreferredSize(dimension);
        setBackground(Color.GRAY);
        mBalls = balls;
        mPlayers = players;
        mPolygon = polygon;
        mGamestate = gameState;
        mTimeRemaining = timeRemaining;
        repaint();
    }

    public void draw(){
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        g.fillPolygon(mPolygon);
        g.setColor(Color.WHITE);
        for (AbstractPlayer player : mPlayers) {
            Paddle paddle = player.getPaddle();
            int[] x = {(int) paddle.getCornerLeftTop().getX(),
                    (int) paddle.getCornerRightTop().getX(),
                    (int) paddle.getCornerRightBottom().getX(),
                    (int) paddle.getCornerLeftBottom().getX()};

            int[] y = {(int) paddle.getCornerLeftTop().getY(),
                    (int) paddle.getCornerRightTop().getY(),
                    (int) paddle.getCornerRightBottom().getY(),
                    (int) paddle.getCornerLeftBottom().getY()};
            g.fillPolygon(x, y, 4);

            // Debug info
            g.setColor(Color.GREEN);
            int[] x2 = {(int) paddle.getCornerLeftTop().getX(),
                    (int) paddle.getCornerRightTop().getX(),
                    (int) paddle.getCornerRightBottom().getX()};

            int[] y2 = {(int) paddle.getCornerLeftTop().getY(),
                    (int) paddle.getCornerRightTop().getY(),
                    (int) paddle.getCornerRightBottom().getY(),};
            g.fillPolygon(x2, y2, 3);
            g.setColor(Color.WHITE);
        }
        for (AbstractBall ball : mBalls) {
            g.fillOval((int) ball.getCoordinate().getX(), (int) ball.getCoordinate().getY(), ball.getDiameter(),
                    ball.getDiameter());
        }
        for (int i = 0; i < mPlayers.size(); i++) {
            char[] characters = String.valueOf(mPlayers.get(i).getScore()).toCharArray();
            g.drawChars(characters, 0, characters.length, (loadScreenWidth() / (mPlayers.size() + 1)) * (i + 1),
                    SCORE_HEIGHT_OFFSET);
        }
    }
}
