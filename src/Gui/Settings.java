package Gui;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import java.util.prefs.Preferences;

import static Constants.Constants.*;

/**
 * Created by L. Keijzer on 08-Nov-16.
 */
public class Settings extends JFrame implements ChangeListener, ActionListener {
    static private Preferences mPreferences = Preferences.userNodeForPackage(Settings.class);
    private GridBagConstraints c;
    private boolean mAddPlayerMenuOpen = false;

    // ComboBoxes
    private JComboBox<String> mComboBoxAddPlayer;

    // Buttons
    private JButton mButtonReset;
    private JButton mButtonExit;
    private JButton mButtonAddPlayer;

    // Panels
    private JPanel mPanelPlayer;
    private JPanel mPanelSliders;
    private JPanel mPanelButtonRow;

    // Paddle
    private JSlider mSliderPaddleMoveSpeed;         // Between 1 and 100, divide by 100 when storing
    private JLabel mLabelPaddleMoveSpeed;
    private JSlider mSliderPaddleWidth;            // Between 1 and 100, in percent of the length of a side
    private JLabel mLabelPaddleWidth;
    private JSlider mSliderPaddleHeight;           // Between 1 and 100, in percent of the paddleWidth
    private JLabel mLabelPaddleHeight;
    private JSlider mSliderPaddleMaxAngleAdjust;   // Between 0 and 90
    private JLabel mLabelPaddleMaxAngleAdjust;

    // Ball
    private JSlider mSliderBallStartMoveSpeed;     // Between 1 and 100, divide by 100 when storing
    private JLabel mLabelBallStartMoveSpeed;
    private JSlider mSliderBallMaxMoveSpeed;       // Between 1 and 200, more than startMoveSpeed, divide by 100
    private JLabel mLabelBallMaxMoveSpeed;
    // when storing
    private JSlider mSliderBallIncreaseMoveSpeed;  // Between 1 and 100, divide by 100 when storing
    private JLabel mLabelBallIncreaseMoveSpeed;
    private JSlider mSliderAmountOfBalls;      // Between 1 and 100
    private JLabel mLabelBallAmountOfBalls;
    private JSlider mSliderBallDiameter;           // Between 1 and 100, in pixels
    private JLabel mLabelBallDiameter;
    private JSlider mSliderBallDelayStartMoving;   // Between 0 and 10, in seconds so store * 1000
    private JLabel mLabelBallDelayStartMoving;

    // Window
    private JSlider mSliderWindowWidth;            // Between 500 and 4000
    private JLabel mLabelWindowWidth;
    private JSlider mSliderWindowHeight;           // Between 500 and 4000
    private JLabel mLabelWindowHeight;
    private JSlider mSliderTargetFps;              // Between 1 and 1000
    private JLabel mLabelTargetFps;

    // Players
    private JSlider mSliderGameAmountOfPlayers;    // Between 2 and 20 // TODO: change min to 1 when implementing sp
    private JComboBox mComboBox;                   // To choose


    Settings(WindowListener windowListener) {
        // Settings for the frame
        addWindowListener(windowListener);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(true);
        c = new GridBagConstraints();
        setLayout(new GridBagLayout());

        // Lists
        // TODO: properly load data for options of different players
        /*
        * Add all types of players in this list*/
        String[] differentPlayers = {PLAYER_KEYBOARD_NAME};
//        mListAddPlayerChoice = new JList<>(data);
        mComboBoxAddPlayer = new JComboBox<>(differentPlayers);

        // Buttons
        mButtonAddPlayer = new JButton(TEXT_BUTTON_ADD_PLAYER);
        mButtonAddPlayer.addActionListener(this);
        mButtonReset = new JButton(TEXT_BUTTON_RESET);
        mButtonReset.addActionListener(this);
        mButtonExit = new JButton(TEXT_BUTTON_EXIT);
        mButtonExit.addActionListener(this);

        mPanelButtonRow = new JPanel(new GridBagLayout());
        c.gridx = 0;
        c.gridy = 0;
        mPanelButtonRow.add(mButtonReset, c);
        c.gridx = 1;
        mPanelButtonRow.add(mButtonExit, c);
        c.gridx = 2;
        mPanelButtonRow.add(mComboBoxAddPlayer, c);
        c.gridx = 3;
        mPanelButtonRow.add(mButtonAddPlayer, c);


        // Load all settings
        // Paddle
        mSliderPaddleMoveSpeed = new JSlider(JSlider.HORIZONTAL, 0, 100, Math.round(loadPaddleMoveSpeed() * 100));
        setSliderDefaults(mSliderPaddleMoveSpeed, true);
        mLabelPaddleMoveSpeed = new JLabel(TEXT_PADDLE_MOVE_SPEED);
        setLabelDefaults(mLabelPaddleMoveSpeed);

        mSliderPaddleWidth = new JSlider(JSlider.HORIZONTAL, 0, 100,
                loadPaddleWidthPercentage());
//                10);
        setSliderDefaults(mSliderPaddleWidth, true);
        mLabelPaddleWidth = new JLabel(TEXT_PADDLE_WIDTH);
        setLabelDefaults(mLabelPaddleWidth);

        mSliderPaddleHeight = new JSlider(JSlider.HORIZONTAL, 0, 100,
                loadPaddleHeightPercentage());
//                10);
        setSliderDefaults(mSliderPaddleHeight, true);
        mLabelPaddleHeight = new JLabel(TEXT_PADDLE_HEIGHT);
        setLabelDefaults(mLabelPaddleHeight);

        mSliderPaddleMaxAngleAdjust = new JSlider(JSlider.HORIZONTAL, 0, 90, loadPaddleMaxAngleAdjust());
        setSliderDefaults(mSliderPaddleMaxAngleAdjust, true);
        mLabelPaddleMaxAngleAdjust = new JLabel(TEXT_PADDLE_MAX_TOTAL_ANGLE_ADJUST);
        setLabelDefaults(mLabelPaddleMaxAngleAdjust);

        // Ball
        mSliderBallStartMoveSpeed = new JSlider(JSlider.HORIZONTAL, 0, 100, Math.round(loadBallStartMoveSpeed() * 100));
        setSliderDefaults(mSliderBallStartMoveSpeed, true);
        mLabelBallStartMoveSpeed = new JLabel(TEXT_BALL_START_MOVE_SPEED);
        setLabelDefaults(mLabelBallStartMoveSpeed);

        mSliderBallMaxMoveSpeed = new JSlider(JSlider.HORIZONTAL, 0, 200, Math.round(loadBallMaxMoveSpeed() * 100));
        setSliderDefaults(mSliderBallMaxMoveSpeed, true, 20);
        mLabelBallMaxMoveSpeed = new JLabel(TEXT_BALL_MAX_SPEED);
        setLabelDefaults(mLabelBallMaxMoveSpeed);

        mSliderBallIncreaseMoveSpeed = new JSlider(JSlider.HORIZONTAL, 0, 100, Math.round(loadBallIncreaseMoveSpeed() * 100));
        setSliderDefaults(mSliderBallIncreaseMoveSpeed, true);
        mLabelBallIncreaseMoveSpeed = new JLabel(TEXT_BALL_INCREASE_SPEED);
        setLabelDefaults(mLabelBallIncreaseMoveSpeed);

        mSliderAmountOfBalls = new JSlider(JSlider.HORIZONTAL, 0, 100, loadAmountOfBalls());
        setSliderDefaults(mSliderAmountOfBalls, true);
        mLabelBallAmountOfBalls = new JLabel(TEXT_AMOUNT_OF_BALLS);
        setLabelDefaults(mLabelBallAmountOfBalls);

        mSliderBallDiameter = new JSlider(JSlider.HORIZONTAL, 0, 100, loadBallDiameter());
        setSliderDefaults(mSliderBallDiameter, true);
        mLabelBallDiameter = new JLabel(TEXT_BALL_DIAMETER);
        setLabelDefaults(mLabelBallDiameter);

        mSliderBallDelayStartMoving = new JSlider(JSlider.HORIZONTAL, 0, 10, loadBallDelayStartMoving() / 1000);
        setSliderDefaults(mSliderBallDelayStartMoving, true, 1);
        mLabelBallDelayStartMoving = new JLabel(TEXT_TIME_DELAY_BALL_MOVING);
        setLabelDefaults(mLabelBallDelayStartMoving);

        // Window
        mSliderWindowWidth = new JSlider(JSlider.HORIZONTAL, 500, 4000, loadScreenWidth());
        setSliderDefaults(mSliderWindowWidth, true, 500);
        mLabelWindowWidth = new JLabel(TEXT_SCREEN_WIDTH);
        setLabelDefaults(mLabelWindowWidth);

        mSliderWindowHeight = new JSlider(JSlider.HORIZONTAL, 500, 4000, loadScreenHeight());
        setSliderDefaults(mSliderWindowHeight, true, 500);
        mLabelWindowHeight = new JLabel(TEXT_SCREEN_HEIGHT);
        setLabelDefaults(mLabelWindowHeight);

        mSliderTargetFps = new JSlider(JSlider.HORIZONTAL, 0, 1000, loadTargetFps());
        setSliderDefaults(mSliderTargetFps, true, 200);
        mLabelTargetFps = new JLabel(TEXT_TARGET_FPS);
        setLabelDefaults(mLabelTargetFps);

        mPanelSliders = new JPanel(new GridBagLayout());
        // Add sliders to mPanelSliders
        c.gridx = 1;
        c.gridy = 0;
        mPanelSliders.add(mSliderPaddleMoveSpeed, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelPaddleMoveSpeed, c);
        c.gridx = 1;
        c.gridy = 1;
        mPanelSliders.add(mSliderPaddleWidth, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelPaddleWidth, c);
        c.gridx = 1;
        c.gridy = 2;
        mPanelSliders.add(mSliderPaddleHeight, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelPaddleHeight, c);
        c.gridx = 1;
        c.gridy = 3;
        mPanelSliders.add(mSliderPaddleMaxAngleAdjust, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelPaddleMaxAngleAdjust, c);
        c.gridx = 1;
        c.gridy = 4;
        mPanelSliders.add(mSliderBallStartMoveSpeed, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelBallStartMoveSpeed, c);
        c.gridx = 1;
        c.gridy = 5;
        mPanelSliders.add(mSliderBallMaxMoveSpeed, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelBallMaxMoveSpeed, c);
        c.gridx = 1;
        c.gridy = 6;
        mPanelSliders.add(mSliderBallIncreaseMoveSpeed, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelBallIncreaseMoveSpeed, c);
        c.gridx = 1;
        c.gridy = 7;
        mPanelSliders.add(mSliderAmountOfBalls, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelBallAmountOfBalls, c);
        c.gridx = 1;
        c.gridy = 8;
        mPanelSliders.add(mSliderBallDiameter, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelBallDiameter, c);
        c.gridx = 1;
        c.gridy = 9;
        mPanelSliders.add(mSliderBallDelayStartMoving, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelBallDelayStartMoving, c);
        c.gridx = 1;
        c.gridy = 10;
        mPanelSliders.add(mSliderWindowWidth, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelWindowWidth, c);
        c.gridx = 1;
        c.gridy = 11;
        mPanelSliders.add(mSliderWindowHeight, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelWindowHeight, c);
        c.gridx = 1;
        c.gridy = 12;
        mPanelSliders.add(mSliderTargetFps, c);
        c.gridx = 0;
        mPanelSliders.add(mLabelTargetFps, c);
//        c.gridx = 0;
//        c.gridy = 13;

        // Add mPanelSliders to frame
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = 0;
        add(mPanelSliders, c);
        c.gridy = 1;
        add(mPanelButtonRow, c);
        pack();
        setMinimumSize(new Dimension(getWidth() + 20, getHeight() + 20));
        setLocation((int) (SCREEN_WIDTH / 2 - getWidth() / 2), (int) (SCREEN_HEIGHT / 2 - getHeight() / 2));
        setVisible(true);

    }

    private void setSliderDefaults(JSlider slider, boolean withIntervals, int intervals) {
        slider.addChangeListener(this);
        if (withIntervals) {
            slider.setMinorTickSpacing(intervals / 2);
            slider.setMajorTickSpacing(intervals);
            slider.setPaintTicks(true);
            slider.setPaintLabels(true);
            slider.setLabelTable(slider.createStandardLabels(intervals));
            slider.setPreferredSize(new Dimension(400, WINDOW_ITEMS_HIEGHT));
        } else {
            slider.setPreferredSize(new Dimension(400, WINDOW_ITEMS_HIEGHT));
        }
    }

    private void resetData() {
        // Set paddles to default values
            // Window
        mSliderWindowWidth.setValue(DEFAULT_SCREEN_WIDTH);
        mSliderWindowHeight.setValue(DEFAULT_SCREEN_HEIGHT);
        mSliderTargetFps.setValue(DEFAULT_TARGET_FPS);
            // Paddle
        mSliderPaddleMoveSpeed.setValue(Math.round(DEFAULT_PADDLE_MOVE_SPEED * 100f));
        mSliderPaddleHeight.setValue(DEFAULT_PADDLE_HEIGHT);
        mSliderPaddleWidth.setValue(DEFAULT_PADDLE_WIDTH);
        mSliderPaddleMaxAngleAdjust.setValue(DEFAULT_PADDLE_MAX_TOTAL_ANGLE_ADJUST);
            // Ball
        mSliderBallStartMoveSpeed.setValue(Math.round(DEFAULT_BALL_START_MOVE_SPEED * 100f));
        mSliderBallDiameter.setValue(DEFAULT_BALL_DIAMETER);
        mSliderAmountOfBalls.setValue(DEFAULT_AMOUNT_OF_BALLS);
        mSliderBallMaxMoveSpeed.setValue(Math.round(DEFAULT_BALL_MAX_MOVE_SPEED * 100f));
        mSliderBallIncreaseMoveSpeed.setValue(Math.round(DEFAULT_BALL_INCREASE_MOVE_SPEED * 100f));
            // Game
        mSliderBallDelayStartMoving.setValue(Math.round(DEFAULT_TIME_DELAY_BALL_MOVING / 1000));
    }

    private void openAddPlayerMenu() {
        mAddPlayerMenuOpen = true;

    }

    private void setSliderDefaults(JSlider slider, boolean withIntervals) {
        setSliderDefaults(slider, withIntervals, 10);
    }

    private void setLabelDefaults(JLabel label) {
        label.setHorizontalAlignment(JLabel.LEFT);
        label.setVerticalAlignment(JLabel.TOP);
        label.setPreferredSize(new Dimension(200, WINDOW_ITEMS_HIEGHT));
    }

    // 0f - maxFloat
    private void storePaddleMoveSpeed(float speed) {
        if (speed < 0) speed = 0f;
        mPreferences.putFloat(PREF_PADDLE_MOVE_SPEED, speed);
        System.out.println("Storing paddleMoveSpeed");
    }

    // 1 - 100
    public static void storePaddleWidth(int width) {
        if (width < 1) width = 1;
        if (width > 100) width = 100;
        // Load paddleHeightPercentage for later use
        int tempHeight = loadPaddleHeightPercentage();
        // Store the width of a paddle based on the height of the screen
        mPreferences.putInt(PREF_PADDLE_WIDTH, Math.round((float) loadScreenHeight() * ((float) width / 100f)));
        // Update paddleHeight
        storePaddleHeight(tempHeight);
    }

    // 1 - 100
    public static void storePaddleHeight(int height) {
        if (height < 1) height = 1;
        if (height > 100) height = 100;
        // Store the height of a paddle based on the width of the paddle
        mPreferences.putInt(PREF_PADDLE_HEIGHT, Math.round((float) loadPaddleWidthAbsolute() * ((float) height /  100f)));
    }

    // 0 - 90
    private void storePaddleMaxAngleAdjust(int angle) {
        if (angle < 0) angle = 0;
        if (angle > 90) angle = 90;
        mPreferences.putInt(PREF_PADDLE_MAX_TOTAL_ANGLE_ADJUST, angle);
    }

    // 0.01f - maxFloat
    private void storeBallStartMoveSpeed(float speed) {
        if (speed < 0.01) speed = 0.01f;
        // First store to prevent a deadlock loop
        mPreferences.putFloat(PREF_BALL_START_MOVE_SPEED, speed);
        if (speed > mPreferences.getFloat(PREF_BALL_MAX_SPEED, DEFAULT_BALL_MAX_MOVE_SPEED)) {
            storeBallMaxMoveSpeed(speed);
            mSliderBallMaxMoveSpeed.setValue(Math.round(speed * 100));
        }
    }

    // 0.01f - maxFloat
    private void storeBallMaxMoveSpeed(float speed) {
        if (speed < 0.01f) speed = 0.01f;
        mPreferences.putFloat(PREF_BALL_MAX_SPEED, speed);
        if (speed < mPreferences.getFloat(PREF_BALL_START_MOVE_SPEED, DEFAULT_BALL_START_MOVE_SPEED)) {
            storeBallStartMoveSpeed(speed);
            mSliderBallStartMoveSpeed.setValue(Math.round(speed * 100));
        }
    }

    // 0.01f - maxFloat
    static public void storeBallIncreaseMoveSpeed(float speed) {
        if (speed < 0.01) speed = 0.01f;
        mPreferences.putFloat(PREF_BALL_INCREASE_SPEED, speed);
    }

    // 1 - maxInt
    static public void storeBallAmountOfBalls(int amount) {
        if (amount < 1) amount = 1;
        mPreferences.putInt(PREF_AMOUNT_OF_BALLS, amount);
    }

    // 1 - maxInt
    static public void storeBallDiameter(int diameter) {
        if (diameter < 1) diameter = 1;
        mPreferences.putInt(PREF_BALL_DIAMETER, diameter);
    }

    // 0 - 10000
    static public void storeBallDelayStartMoving(int delay) {
        if (delay < 0) delay = 0;
        mPreferences.putInt(PREF_TIME_DELAY_BALL_MOVING, delay);
    }

    // 500 - 4000
    static public void storeScreenWidth(int width) {
        if (width < 500) width = 500;
        if (width > 4000) width = 4000;
        mPreferences.putInt(PREF_SCREEN_WIDTH, width);
    }

    // 500 - 10000
    static public void storeScreenHeight(int height) {
        if (height < 500) height = 500;
        if (height > 4000) height = 4000;
        // Load paddleWidthPercentage
        int tempWidth = loadPaddleWidthPercentage();
        mPreferences.putInt(PREF_SCREEN_HEIGHT, height);
        // Update paddle width
        storePaddleWidth(tempWidth);
    }

    // 1 - 1000
    static public void storeFPS(int fps) {
        if (fps < 1) fps = 1;
        if (fps > 1000) fps = 1000;
        mPreferences.putInt(PREF_TARGET_FPS, fps);
    }


    // Methods to load the data
    static public float loadPaddleMoveSpeed() {
        return mPreferences.getFloat(PREF_PADDLE_MOVE_SPEED, DEFAULT_PADDLE_MOVE_SPEED);
    }

    static public int loadPaddleWidthAbsolute() {
        return mPreferences.getInt(PREF_PADDLE_WIDTH, DEFAULT_PADDLE_WIDTH);
    }

    static public int loadPaddleHeightAbsolute() {
        return mPreferences.getInt(PREF_PADDLE_HEIGHT, DEFAULT_PADDLE_HEIGHT);
    }

    static public int loadPaddleMaxAngleAdjust() {
        return mPreferences.getInt(PREF_PADDLE_MAX_TOTAL_ANGLE_ADJUST, DEFAULT_PADDLE_MAX_TOTAL_ANGLE_ADJUST);
    }

    static public float loadBallStartMoveSpeed() {
        return mPreferences.getFloat(PREF_BALL_START_MOVE_SPEED, DEFAULT_BALL_START_MOVE_SPEED);
    }

    static public float loadBallMaxMoveSpeed() {
        return mPreferences.getFloat(PREF_BALL_MAX_SPEED, DEFAULT_BALL_MAX_MOVE_SPEED);
    }

    static public float loadBallIncreaseMoveSpeed() {
        return mPreferences.getFloat(PREF_BALL_INCREASE_SPEED, DEFAULT_BALL_INCREASE_MOVE_SPEED);
    }

    static public int loadAmountOfBalls() {
        return mPreferences.getInt(PREF_AMOUNT_OF_BALLS, DEFAULT_AMOUNT_OF_BALLS);
    }

    static public int loadBallDiameter() {
        return mPreferences.getInt(PREF_BALL_DIAMETER, DEFAULT_BALL_DIAMETER);
    }

    static public int loadBallDelayStartMoving() {
        return mPreferences.getInt(PREF_TIME_DELAY_BALL_MOVING, DEFAULT_TIME_DELAY_BALL_MOVING);
    }

    static public int loadScreenWidth() {
        return mPreferences.getInt(PREF_SCREEN_WIDTH, DEFAULT_SCREEN_WIDTH);
    }

    static public int loadScreenHeight() {
        return mPreferences.getInt(PREF_SCREEN_HEIGHT, DEFAULT_SCREEN_HEIGHT);
    }

    static public int loadTargetFps() {
        return mPreferences.getInt(PREF_TARGET_FPS, DEFAULT_TARGET_FPS);
    }

    static private int loadPaddleWidthPercentage() {
        return Math.round(((float) loadPaddleWidthAbsolute() * 100f) / (float) loadScreenHeight());
    }

    static private int loadPaddleHeightPercentage() {
        return Math.round(((float) loadPaddleHeightAbsolute() * 100f) / (float) loadPaddleWidthAbsolute());
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        Object src = e.getSource();

        if (src == mSliderPaddleMoveSpeed) {
            storePaddleMoveSpeed(mSliderPaddleMoveSpeed.getValue() / 100f);
        } else if (src == mSliderPaddleWidth) {
            storePaddleWidth(mSliderPaddleWidth.getValue());
        } else if (src == mSliderPaddleHeight) {
            storePaddleHeight(mSliderPaddleHeight.getValue());
        } else if (src == mSliderPaddleMaxAngleAdjust) {
            storePaddleMaxAngleAdjust(mSliderPaddleMaxAngleAdjust.getValue());
        } else if (src == mSliderBallStartMoveSpeed) {
            storeBallStartMoveSpeed(mSliderBallStartMoveSpeed.getValue() / 100f);
        } else if (src == mSliderBallMaxMoveSpeed) {
            storeBallMaxMoveSpeed(mSliderBallMaxMoveSpeed.getValue() / 100f);
        } else if (src == mSliderBallIncreaseMoveSpeed) {
            storeBallIncreaseMoveSpeed(mSliderBallIncreaseMoveSpeed.getValue() / 100f);
        } else if (src == mSliderAmountOfBalls) {
            storeBallAmountOfBalls(mSliderAmountOfBalls.getValue());
        } else if (src == mSliderBallDiameter) {
            storeBallDiameter(mSliderBallDiameter.getValue());
        } else if (src == mSliderBallDelayStartMoving) {
            storeBallDelayStartMoving(mSliderBallDelayStartMoving.getValue() * 1000);
        } else if (src == mSliderWindowWidth) {
            storeScreenWidth(mSliderWindowWidth.getValue());
        } else if (src == mSliderWindowHeight) {
            storeScreenHeight(mSliderWindowHeight.getValue());
        } else if (src == mSliderTargetFps) {
            storeFPS(mSliderTargetFps.getValue());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();

        if (src == mButtonReset) {
            resetData();
        } else if (src == mButtonExit) {
            dispose();
        } else if (src == mButtonAddPlayer) {
            if (!mAddPlayerMenuOpen) {
                openAddPlayerMenu();
            }
        }
    }
}
