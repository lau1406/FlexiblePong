package Gui;

import Game.Game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.prefs.Preferences;

import static Constants.Constants.*;

/**
 * Created by L. Keijzer on 27-Oct-16.
 */

public class MainMenu extends JFrame implements ActionListener, WindowListener {
    private Preferences mPreferences = Preferences.userNodeForPackage(Settings.class);
    private boolean mGameIsRunning = false;
    private boolean mSettingsMenuOpen = false;
    private boolean mInterwebsMenuOpen = false;

    private JPanel mJPanelMain;
    private JButton mButtonLocal;
    private JButton mButtonInterwebs;
    private JButton mButtonSettings;
    private JButton mButtonExit;
    private GridBagConstraints c;

    private Game mGame;
    private Settings mSettings;
    private Thread mThreadGame;

    private MainMenu() {
        c = new GridBagConstraints();
        // TODO: add main menu and host/client options
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("Pong Main Menu");


        mJPanelMain = new JPanel(new GridBagLayout());
        mButtonLocal = new JButton(BUTTON_LOCAL_NAME);
        mButtonInterwebs = new JButton(BUTTON_INTERWEBS_NAME);
        mButtonSettings = new JButton(BUTTON_SETTINGS_NAME);
        mButtonExit = new JButton(BUTTON_EXIT_NAME);

        mButtonLocal.addActionListener(this);
        mButtonInterwebs.addActionListener(this);
        mButtonSettings.addActionListener(this);
        mButtonExit.addActionListener(this);

        c.gridx = 0;
        c.gridy = 2;
        mJPanelMain.add(mButtonInterwebs, c);
        c.gridx = 1;
        mJPanelMain.add(mButtonLocal, c);
        c.gridx = 2;
        mJPanelMain.add(mButtonSettings, c);
        c.gridx = 3;
        mJPanelMain.add(mButtonExit, c);


        add(mJPanelMain);
        pack();
        setMinimumSize(new Dimension(getWidth() + 20, getHeight() + 20));
        setLocation((int) (SCREEN_WIDTH / 2 - getWidth() / 2), (int) (SCREEN_HEIGHT / 2 - getHeight() / 2));
        setVisible(true);

        // Start a new game
//        new Game();
    }

    public static void main(String[] args) {
        new MainMenu();
    }

    private void startGame() {
        if (!mGameIsRunning && !mSettingsMenuOpen && !mInterwebsMenuOpen) {
            mThreadGame = new Thread(() -> {
                mGameIsRunning = true;
                mGame = new Game(this);
            });
            mThreadGame.start();
        }
    }

    private void openSettings() {
        if (!mGameIsRunning && !mSettingsMenuOpen && !mInterwebsMenuOpen) {
            mSettingsMenuOpen = true;
            mSettings = new Settings(this);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();

        if (src == mButtonInterwebs) {
            // TODO: start interwebs things
        } else if (src == mButtonLocal) {
            startGame();
        } else if (src == mButtonSettings) {
            openSettings();
        } else if (src == mButtonExit) {
            dispose();
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {
        Object src = e.getSource();

        if (mSettings != null) {
            if (src == mSettings) {
                mSettingsMenuOpen = false;
            }
        }

        if (mThreadGame != null) {
            if (mThreadGame.isAlive()) {
                System.out.println("alive");
                mThreadGame.interrupt();
                mThreadGame = null;
                mGameIsRunning = false;
            }
        }
        if (mThreadGame != null) {
            System.out.println("!null");
        }

//        if (mThreadGame.isAlive()) {
//            System.out.println("closing");
//            mThreadGame = null;
//            mGameIsRunning = false;
//            // TODO: continue here with the closing of a game etc
//
//        }
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
