package Players;

import Objects.Paddle;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import static Constants.Constants.PLAYER_KEYBOARD_NAME;

/**
 * Created by L. Keijzer on 29-Oct-16.
 */
public class PlayerKeyboard extends AbstractPlayer implements KeyListener {
    private final int LEFT_KEY;     // Moving left, keyCode from keyboard key
    private final int RIGHT_KEY;    // Moving right, keyCode from keyboard key
    private final JFrame mJFrame;
    // TODO: if not used maybe remove list with pressed keys
    private List<Integer> mPressedKeys;

    public PlayerKeyboard(Paddle paddle, int LEFT_KEY, int RIGHT_KEY, JFrame frame, int number) {
        super(paddle, number);
        mPressedKeys = new ArrayList<>();
        this.LEFT_KEY = LEFT_KEY;
        this.RIGHT_KEY = RIGHT_KEY;
        mJFrame = frame;
        mJFrame.addKeyListener(this);
    }

    public int getLEFT_KEY() {
        return LEFT_KEY;
    }

    public int getRIGHT_KEY() {
        return RIGHT_KEY;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (mPressedKeys.indexOf(e.getKeyCode()) != -1) {
            // Old press
            return;
        }

        // New press
        int keyCode = e.getKeyCode();
        mPressedKeys.add(keyCode);
        if (keyCode == LEFT_KEY ) {
            if (getDirection() == Direction.RIGHT) {
                setDirection(Direction.BOTH);
            } else {
                setDirection(Direction.LEFT);
            }
            getPaddle().setStartMoveTime(System.currentTimeMillis());
        } else if (keyCode == RIGHT_KEY) {
            if (getDirection() == Direction.LEFT) {
                setDirection(Direction.BOTH);
            } else {
                setDirection(Direction.RIGHT);
            }
            getPaddle().setStartMoveTime(System.currentTimeMillis());
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int position = mPressedKeys.indexOf(e.getKeyCode());
        if (position != -1) {
            mPressedKeys.remove(position);
        }

        if (e.getKeyCode() == LEFT_KEY) {
            if (getDirection() == Direction.BOTH) {
                setDirection(Direction.RIGHT);
            } else {
                setDirection(Direction.NONE);
            }
        } else if (e.getKeyCode() == RIGHT_KEY) {
            if (getDirection() == Direction.BOTH) {
                setDirection(Direction.LEFT);
            } else {
                setDirection(Direction.NONE);
            }
        }
    }

    @Override
    public String getName() {
        return PLAYER_KEYBOARD_NAME;
    }
}
