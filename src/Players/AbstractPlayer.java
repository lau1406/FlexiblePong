package Players;

import Objects.Paddle;

import java.awt.*;

import static Constants.Constants.*;
import static Constants.Settings.*;
import static Gui.Settings.loadPaddleMoveSpeed;

/**
 * Created by L. Keijzer on 29-Oct-16.
 */

/**
 * Extend this class to create a player
 * In this player change @code{mDirection} to tell the move() method in what direction the paddle needs to move.*/
abstract public class AbstractPlayer {
    final int PLAYER_NUMBER;

    enum Direction {
        LEFT,
        RIGHT,
        NONE,
        BOTH
    }

    private int score = 0;
    private Paddle mPaddle;
    private Direction mDirection = Direction.NONE;


    AbstractPlayer(Paddle paddle, int number) {
        mPaddle = paddle;
        PLAYER_NUMBER = number;
    }

    /**
     * Return a name of this type of player, for indexing
     * */
    abstract public String getName();

    /**
     * Moves the paddle in the correct direction
     * */
    public void move(Polygon polygon) {
        float amount, xDif, yDif;
        switch (mDirection) {
            case LEFT:
                amount = (System.currentTimeMillis() - getPaddle().getStartMoveTime()) * loadPaddleMoveSpeed();

                xDif = (float) Math.sin(Math.toRadians(getPaddle().getAngleMain())) * amount;
                yDif = (float) Math.cos(Math.toRadians(getPaddle().getAngleMain())) * amount * -1;
                if (Math.abs(xDif) < VERY_SMALL_NUMBER) {
                    xDif = 0f;
                }
                if (Math.abs(yDif) < VERY_SMALL_NUMBER) {
                    yDif = 0f;
                }

                getPaddle().move(xDif, yDif, polygon);
                getPaddle().setStartMoveTime(System.currentTimeMillis());
                break;
            case RIGHT:
                amount = (System.currentTimeMillis() - getPaddle().getStartMoveTime()) * loadPaddleMoveSpeed();

                xDif = (float) Math.sin(Math.toRadians(getPaddle().getAngleMain())) * amount * -1;
                yDif = (float) Math.cos(Math.toRadians(getPaddle().getAngleMain())) * amount;

                getPaddle().move(xDif, yDif, polygon);
                getPaddle().setStartMoveTime(System.currentTimeMillis());
                break;
            default:
                getPaddle().setStartMoveTime(System.currentTimeMillis());
        }
    }

    public void increaseScore() {
        score++;
    }

    Direction getDirection() {
        return mDirection;
    }

    void setDirection(Direction direction) {
        mDirection = direction;
    }

    public int getScore() {
        return score;
    }

    public Paddle getPaddle() {
        return mPaddle;
    }

    public int getPLAYER_NUMBER() {
        return PLAYER_NUMBER;
    }
}
