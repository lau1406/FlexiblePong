package Balls;// Project Pong
// As created by Laurence Keijzer on 07-Nov-16
// Created using IntelliJ IDEA


import Objects.Coordinate;

public class NormalBall extends AbstractBall {

	public NormalBall( int diameter, Coordinate coordinate, float angle ) {
		super(diameter, coordinate, angle);
	}
}
