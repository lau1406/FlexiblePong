package Balls;

import Constants.CustomMath;
import Objects.Coordinate;
import Objects.Paddle;
import Players.AbstractPlayer;

import static Constants.Constants.SMALL_NUMBER;
import static Constants.Settings.*;
import static Gui.Settings.*;

/**
 * Created by L. Keijzer on 06-Nov-16.
 */
abstract public class AbstractBall {
    // TODO: remove internal calls to getters or setters?
    private int mDiameter;
    private Coordinate mCoordinate;     // Top left coordinates
    private float mSpeed;
    private float mAngle;     // Between 0 and 360 degrees, 90 degrees is right
    private long mStartMoveTime;        // Last moment in time the ball was moved. This to make moving objects
    // frame-rate independent


    AbstractBall(int diameter, Coordinate coordinate, float angle) {
        mDiameter = diameter;
        mCoordinate = coordinate;
        mSpeed = 0f;
        // TODO: maybe start with going to a random player?
        mAngle = angle;
        mStartMoveTime = System.currentTimeMillis();
    }

    public void resetBall(AbstractPlayer playerThatLost) {
        // The ball shoots towards the center of the player that lost
        // TODO: check math, could be slightly off considering mCoordinate is the coordinate of the top left corner
        Paddle paddle = playerThatLost.getPaddle();
        float xDif = ((paddle.getCornerLeftTop().getX() - paddle.getCornerRightBottom().getX()) / 2 + paddle
                .getCornerRightBottom().getX()) - (loadScreenWidth() / 2);
        float yDif = ((paddle.getCornerLeftTop().getY() - paddle.getCornerRightBottom().getY()) / 2 + paddle
                .getCornerRightBottom().getY()) - (loadScreenHeight() / 2);
        mAngle = (float) ((Math.toDegrees(Math.atan2(yDif, xDif)) + 90) % 360);

        mCoordinate.setX(loadScreenWidth() / 2 - mDiameter);
        mCoordinate.setY(loadScreenHeight() / 2 - mDiameter);
        mSpeed = loadBallStartMoveSpeed();
//        increaseSpeed();    // TODO: continue here
        mStartMoveTime = System.currentTimeMillis();
    }

    public boolean checkCollision(Paddle paddle) {
        // Check multiple points on the ball, based on the diameter of the ball
        Coordinate centerCoordinate = new Coordinate(mCoordinate.getX() + mDiameter / 2, mCoordinate.getY() +
                mDiameter / 2);
        Coordinate pointOnBall = new Coordinate(0, 0);
        for (int i = 0; i < loadBallDiameter(); i++) {
            pointOnBall.setX(centerCoordinate.getX() + (float) Math.sin(Math.toRadians((360f / loadBallDiameter()) * i)) *
                    loadBallDiameter() / 2);
            pointOnBall.setY(centerCoordinate.getY() + (float) Math.cos(Math.toRadians((360f / loadBallDiameter()) * i)) *
                    loadBallDiameter() / 2);

            // The ball is inside a paddle if the area of the 4 triangles is equal to the area of the paddle
            double area = CustomMath.calcAreaTriangle(pointOnBall, paddle.getCornerLeftTop(), paddle.getCornerRightTop()) +
                    CustomMath.calcAreaTriangle(pointOnBall, paddle.getCornerRightTop(), paddle.getCornerRightBottom()) +
                    CustomMath.calcAreaTriangle(pointOnBall, paddle.getCornerRightBottom(), paddle.getCornerLeftBottom()) +
                    CustomMath.calcAreaTriangle(pointOnBall, paddle.getCornerLeftBottom(), paddle.getCornerLeftTop());

            if (Math.abs(area - paddle.getArea()) < SMALL_NUMBER) {
                // Center of the ball is inside the paddle
                System.out.println("inside");
                // TODO: this only works good if the ball hits a side that is parallel to the angle, aka the top or bottom
                float anglePaddle = paddle.getAngleMain() % 180;
                float angleBall = getAngle() % 180;
                setAngle(((getAngle() - 2 * (angleBall - anglePaddle))) % 360);
                System.out.println("getAngle() = " + getAngle());
                // TODO: set proper coordinates, this just move the ball the width of the paddle forwards
                setXCoordinate(mCoordinate.getX() + (float) (paddle.getHeight() * Math.sin(Math.toRadians(getAngle()))));
                setYCoordinate(mCoordinate.getY() - (float) (paddle.getHeight() * Math.cos(Math.toRadians(getAngle()))));
                increaseSpeed();
                return true;
            }
        }

        return false;
    }

    public void increaseSpeed(float amount) {
        mSpeed += amount;
        if (mSpeed > loadBallMaxMoveSpeed()) {
            mSpeed = loadBallMaxMoveSpeed();
        }
    }

    public void increaseSpeed() {
        mSpeed += loadBallIncreaseMoveSpeed();
        if (mSpeed > loadBallMaxMoveSpeed()) {
            mSpeed = loadBallMaxMoveSpeed();
        }
        System.out.println("mSpeed = " + mSpeed);
    }

    public void setXCoordinate(float xCoordinate) {
        mCoordinate.setX(xCoordinate);
    }

    public void setYCoordinate(float yCoordinate) {
        mCoordinate.setY(yCoordinate);
    }

    public int getDiameter() {
        return mDiameter;
    }

    public Coordinate getCoordinate() {
        return mCoordinate;
    }

    public void setCoordinate( Coordinate mCoordinate ) {
        this.mCoordinate = mCoordinate;
    }

    public float getSpeed() {
        return mSpeed;
    }

    public void setSpeed(float speed) {
        mSpeed = speed;
        if (mSpeed > loadBallMaxMoveSpeed()) {
            mSpeed = loadBallMaxMoveSpeed();
        }
    }

    public float getAngle() {
        return mAngle;
    }

    public void setAngle(float angle) {
        while (angle < 0) {
            angle = (angle + 360) % 360;
        }
        mAngle = angle;
    }

    public long getStartMoveTime() {
        return mStartMoveTime;
    }

    public void setStartMoveTime(long startMoveTime) {
        mStartMoveTime = startMoveTime;
    }
}
