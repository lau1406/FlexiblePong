package Game;

import Balls.AbstractBall;
import Balls.NormalBall;
import Constants.CustomMath;
import Gui.Field;
import Objects.Coordinate;
import Objects.Paddle;
import Players.AbstractPlayer;
import Players.PlayerKeyboard;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;
import java.util.prefs.Preferences;

import static Constants.Settings.*;
import static Gui.Settings.*;

/**
 * Created by L. Keijzer on 27-Oct-16.
 */

public class Game implements KeyListener {
    private Polygon mPolygon;
    private JFrame mFrame;
    private final Dimension mDimension;
    // TODO: remove mPaddles and store paddles inside player
    private ArrayList<AbstractPlayer> mPlayers;
    private ArrayList<AbstractBall> mBalls;
    private int mGameState = 0;// 0: before game(ball is nog moving), 1: in game(ball is moving), 2: after game(someone
    // won)
    private List<Integer> mPressedKeys;
    private Field mField;

    //    private long mCountdown;    // Time in ms before the game starts
    private long mStartTime;
    private long mRoundStartTime;
    private long mTimeRemainingBeforeRoundStart = loadBallDelayStartMoving();
    private Preferences mPreferences;
//    private WindowListener mWindowListener;

//    private int mBallDelay = TIME_DELAY_BALL_MOVING;


    public Game(WindowListener windowListener) {
//        mPreferences = Preferences.userNodeForPackage(Settings.class);
//        System.out.println("int: " + mPreferences.getInt("test", -1));
        mPressedKeys = new ArrayList<>();

        mFrame = new JFrame("Pong");
        // TODO: later change to dispose on close
        mFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        mFrame.setResizable(false);
        mFrame.setBackground(Color.BLACK);
        mFrame.addWindowListener(windowListener);

        mPlayers = new ArrayList<>();
        // Special case, 2 players, 4 sides
        // TODO: add special case for 4 players
        // TODO: bugcheck AMOUNT_PLAYERS == 4

        // TODO: add special case for 1 lonely player >:-]
        if (AMOUNT_PLAYERS < 2) {
            mDimension = new Dimension(loadScreenWidth(), loadScreenHeight());
            return;
        } else if (AMOUNT_PLAYERS == 2) {
            mDimension = new Dimension(loadScreenWidth(), loadScreenHeight());
            // TODO: replace key assignment
            //noinspection SuspiciousNameCombination
            mPlayers.add(new PlayerKeyboard(new Paddle(
                    new Coordinate(loadPaddleHeightAbsolute(), (loadScreenHeight() / 2) - (loadPaddleWidthAbsolute() / 2)),
                    new Coordinate(0, (loadScreenHeight() / 2) - (loadPaddleWidthAbsolute() / 2)),
                    new Coordinate(loadPaddleHeightAbsolute(), (loadScreenHeight() / 2) + (loadPaddleWidthAbsolute() / 2)),
                    new Coordinate(0, (loadScreenHeight() / 2) + (loadPaddleWidthAbsolute() / 2)),
                    0f, 0), KEY_P1_UP, KEY_P1_DOWN, mFrame, 0));
            mPlayers.add(new PlayerKeyboard(new Paddle(
                    new Coordinate(loadScreenWidth() - loadPaddleHeightAbsolute(), (loadScreenHeight() / 2) + (loadPaddleWidthAbsolute() / 2)),
                    new Coordinate(loadScreenWidth() , (loadScreenHeight() / 2) + (loadPaddleWidthAbsolute() / 2)),
                    new Coordinate(loadScreenWidth() - loadPaddleHeightAbsolute(), (loadScreenHeight() / 2) - (loadPaddleWidthAbsolute() / 2)),
                    new Coordinate(loadScreenWidth() , (loadScreenHeight() / 2) - (loadPaddleWidthAbsolute() / 2)),
                    180f, 1), KEY_P2_DOWN, KEY_P2_UP, mFrame, 1));
            int[] x = {0, 0, loadScreenWidth(), loadScreenWidth()};
            int[] y = {0, loadScreenHeight(), loadScreenHeight(), 0};
            mPolygon = new Polygon(x, y, 4);
        } else {
            // We want a square playing field to put the polygon in
            int smallestSize = Math.min(loadScreenWidth(), loadScreenHeight());
            // To prevent later errors
            storeScreenWidth(smallestSize);
            storeScreenHeight(smallestSize);
            mDimension = new Dimension(loadScreenWidth(), loadScreenHeight());

            // Generate polygon
            int[] x = new int[AMOUNT_PLAYERS];
            int[] y = new int[AMOUNT_PLAYERS];
            for (int i = 0; i <= AMOUNT_PLAYERS; i++) {
                // <= instead of < to go one further and make the last paddle
                float angle = (((360f / AMOUNT_PLAYERS) * i)) % 360;
//                System.out.println("angle = " + angle);
                // Generate all coordinates base on the center point of the frame
                if (i < AMOUNT_PLAYERS) {
                    x[i] = (int) ((loadScreenWidth() / 2) + ((loadScreenWidth() / 2) * Math.sin(Math.toRadians(angle))));
                    y[i] = (int) ((loadScreenHeight() / 2) + ((loadScreenHeight() / 2) * Math.cos(Math.toRadians(angle))));
                }
//                System.out.println("x[i] + y[i] = " + x[i] + ":" + y[i]);

                if (i == 0) {
                    // Skip the first one, we need 2 points to make a paddle easily
                    continue;
                }

//                System.out.println("i = " + i);

                boolean even = AMOUNT_PLAYERS % 2 == 0;
                // Calculate the angle of the paddle
                // Different formulas for even and uneven AMOUNT_PLAYERS
                if (even) {
                    // Even
                    angle = ((540 + 360 / AMOUNT_PLAYERS) - (360 / AMOUNT_PLAYERS) * (i - 1)) % 360;
                } else {
                    // Uneven
                    angle = (450 + ((AMOUNT_PLAYERS / 2) - (i - 1)) * (360f / AMOUNT_PLAYERS)) % 360;
                }
//                System.out.println("angle = " + angle);

                // Distance between point and LeftBottom coordinate
                float length = (float)
                        CustomMath.dist(x[i % AMOUNT_PLAYERS], y[i % AMOUNT_PLAYERS], x[i - 1], y[i - 1]) / 2 -
                        loadPaddleWidthAbsolute() / 2;
//                System.out.println("length = " + length);

                mPlayers.add(new PlayerKeyboard(genPaddle(angle, new Coordinate(x[i - 1], y[i - 1]), length, i - 1),
                        KEY_P1_UP, KEY_P1_DOWN, mFrame, i - 1));
            }
            mPolygon = new Polygon(x, y, AMOUNT_PLAYERS);

        }

        // Create all the balls, for now only normal balls
        mBalls = new ArrayList<>();
        for (int i = 0; i < loadAmountOfBalls(); i++) {
            mBalls.add(new NormalBall(loadBallDiameter(), new Coordinate(loadScreenWidth() / 2 - loadBallDiameter() / 2,
                    loadScreenHeight() / 2 - loadBallDiameter() / 2), 90));
        }

        mField = new Field(mDimension, mBalls, mPlayers, mPolygon, mGameState, mTimeRemainingBeforeRoundStart);
        mFrame.add(mField);

        mFrame.addKeyListener(this);

        mFrame.pack();
        mFrame.setVisible(true);

        gameLoop(loadTargetFps());
    }

    private void gameLoop(final int targetFPS) {
        // Game has started
        mRoundStartTime = System.currentTimeMillis() + loadBallDelayStartMoving();

        while (true) {
            mStartTime = System.currentTimeMillis();

            switch (mGameState) {
                case 0:
                    mTimeRemainingBeforeRoundStart = mRoundStartTime - System.currentTimeMillis();
                    if (mTimeRemainingBeforeRoundStart <= 0) {
                        // Give the ball some speed to start moving and an angle
                        // TODO: change angle base on last person that scored
                        for (AbstractBall ball : mBalls) {
                            ball.setStartMoveTime(System.currentTimeMillis());
                            ball.setSpeed(loadBallStartMoveSpeed());
                        }
//                        mBalls.get(0).setSpeed(1.1f);
                        mGameState = 1;
                    }
                    break;
                case 1:
                    for (AbstractBall ball : mBalls) {
                        moveBall(ball, (System.currentTimeMillis() - ball.getStartMoveTime()) * ball.getSpeed());
                    }
                    break;
                case 2:

                    break;
            }

            // TODO: do calcs

            // Move paddles
            for (AbstractPlayer player : mPlayers) {
                player.move(mPolygon);
            }

            // Draw everything
            mField.draw();

//            try {
//                Thread.sleep(100);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }


            // TODO: sleeping for less than 15ms not rly possible
            // To limit the fps of the game
            long sleepTime = (int) (1000f / targetFPS - (System.currentTimeMillis() - mStartTime));
            // Time the system should take over one frame - the time it has already used. If this is less than 1, the
            // system should not sleep
            if (sleepTime > 0) {
                try {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                    Thread.currentThread().interrupt();
                    System.out.println("Exiting game");
                    return;
                }
            }

//            mFrameTime = System.currentTimeMillis() - mStartTime;   // Time this frame took
//            System.out.println("fps: " + 1000 / (System.currentTimeMillis() - mStartTime));
        }
    }

    private void moveBall(AbstractBall ball, float amount) {
        boolean collision = false;
        if (amount < 0) {
            amount *= -1f;
        }
        float xCoordinate = ball.getCoordinate().getX() + (float) (amount * Math.sin(Math.toRadians(ball.getAngle())));
        float yCoordinate = ball.getCoordinate().getY() - (float) (amount * Math.cos(Math.toRadians(ball.getAngle())));
//        System.out.println("new coordinates: " + xCoordinate + ":" + yCoordinate + ", angle:" + ball.getAngle());

        if (yCoordinate < 0) {
            yCoordinate = 0;
            ball.setAngle((540 - ball.getAngle()) % 360); // 180 + 360 = 540
        } else if (yCoordinate > loadScreenHeight()) {
            yCoordinate = loadScreenHeight() - ball.getDiameter();
            ball.setAngle((540 - ball.getAngle()) % 360); // 180 + 360 = 540
        }

        for (AbstractPlayer player : mPlayers) {
            // TODO: if unstable remove break
            if (ball.checkCollision(player.getPaddle())) {
                collision = true;
                break;
            }
        }

        // TODO: change to take polygon in consideration
        if (xCoordinate < 0) {
            mPlayers.get(1).increaseScore();
            System.out.println(mPlayers.get(0).getScore() + ":" + mPlayers.get(1).getScore());
            ball.resetBall(mPlayers.get(0));
            return;
        } else if (xCoordinate > loadScreenWidth()) {
            mPlayers.get(0).increaseScore();
            System.out.println(mPlayers.get(0).getScore() + ":" + mPlayers.get(1).getScore());
            ball.resetBall(mPlayers.get(1));
            return;
        }

        if (!collision) {
            ball.setXCoordinate(xCoordinate);
            ball.setYCoordinate(yCoordinate);
        }
        ball.setStartMoveTime(System.currentTimeMillis());
    }

    private void changeBallBelocityAndAngle(AbstractBall ball, float speed, int angle) {
        while (angle < 0) {
            angle += 360;
        }
        angle %= 360;
        ball.setAngle(angle);
        ball.setSpeed(speed);
    }

    private Paddle genPaddle(float angle, Coordinate startCoordinate, float initialDistance, int number) {
        float x_coordinate;
        float y_coordinate;

        // LeftBottom
        x_coordinate = startCoordinate.getX() - (float) Math.sin(Math.toRadians(angle)) * initialDistance;
        y_coordinate = startCoordinate.getY() + (float) Math.cos(Math.toRadians(angle)) * initialDistance;
        Coordinate leftBottomCoordinate = new Coordinate(x_coordinate, y_coordinate);

        // RightBottom
        x_coordinate -= Math.sin(Math.toRadians(angle)) * loadPaddleWidthAbsolute();
        y_coordinate += Math.cos(Math.toRadians(angle)) * loadPaddleWidthAbsolute();
        Coordinate rightBottomCoordinate = new Coordinate(x_coordinate, y_coordinate);

        // Change angle to calc distance to RightTop
        angle -= 90;

        // RightTop
        x_coordinate -= Math.sin(Math.toRadians(angle)) * loadPaddleHeightAbsolute();
        y_coordinate += Math.cos(Math.toRadians(angle)) * loadPaddleHeightAbsolute();
        Coordinate rightTopCoordinate = new Coordinate(x_coordinate, y_coordinate);

        // Change angle to calc distance to LeftTop
        angle -= 90;

        // LeftTop
        x_coordinate -= Math.sin(Math.toRadians(angle)) * loadPaddleWidthAbsolute();
        y_coordinate += Math.cos(Math.toRadians(angle)) * loadPaddleWidthAbsolute();
        Coordinate leftTopCoordinate = new Coordinate(x_coordinate, y_coordinate);

        return new Paddle(leftTopCoordinate, leftBottomCoordinate, rightTopCoordinate, rightBottomCoordinate,
                angle + 180, number);
    }

    private boolean checkCollision(AbstractBall ball, Paddle paddle) {
        return false;
        // TODO: do proper check for every corner
        // Get center of the ball
//        float ballX = ball.getXCoordinate() + ball.getDiameter() / 2;
//        float ballY = ball.getYCoordinate() + ball.getDiameter() / 2;
//
//        if (ballX > paddle.getXCoordinate() &&
//                ballX < paddle.getXCoordinate() + paddle.getWidth() &&
//                ballY > paddle.getYCoordinate() &&
//                ballY < paddle.getYCoordinate() + paddle.getHeight()) {
//            // Ball is touching the paddle
//            // TODO: change angle depending on where it hits the paddle
//            // TODO: add rotation to paddle when there are more paddles/players
//            System.out.println("Angle before: " + ball.getAngle());
//            ball.setAngle((360 - ball.getAngle()));
////                    (int) ((((ballX - paddle.getXCoordinate()) / paddle.getWidth()) - 0.5f) *
////                            PADDLE_MAX_TOTAL_ANGLE_ADJUST));
//            System.out.println("ball.getAngle() = " + ball.getAngle());
//            ball.increaseSpeed();
//            // angle has been done mod 360, should be within 0 and 360
//            if (ball.getAngle() <= 90) {
////                ball.setYCoordinate(ball.getYCoordinate() -
////                        (paddle.getXCoordinate() + paddle.getWidth() - ball.getXCoordinate()) /
////                                (float) (Math.toDegrees(Math.atan(ball.getAngle()))));
//                ball.setXCoordinate(paddle.getXCoordinate() + paddle.getWidth());
////                System.out.println("x should be at least: " + (paddle.getXCoordinate() + paddle.getWidth()));
////                System.out.println("Coordinates: " + ball.getXCoordinate() + ":" + ball.getYCoordinate());
////                System.out.println("ball.getAngle() = " + ball.getAngle());
//            } else if (ball.getAngle() <= 180) {
//                int angle = ball.getAngle() - 90;
////                ball.setYCoordinate(ball.getYCoordinate() + (float) ((paddle.getXCoordinate() + paddle.getWidth() -
////                        ball.getXCoordinate()) * Math.tan(Math.toRadians(angle))));
//                ball.setXCoordinate(paddle.getXCoordinate() + paddle.getWidth());
////                System.out.println("x should be at least: " + (paddle.getXCoordinate() + paddle.getWidth()));
////                System.out.println("Coordinates: " + ball.getXCoordinate() + ":" + ball.getYCoordinate());
////                System.out.println("ball.getAngle() = " + ball.getAngle());
//            } else if (ball.getAngle() <= 270) {
//                ball.setXCoordinate(paddle.getXCoordinate() - ball.getDiameter());
////                System.out.println("x should be at most: " + paddle.getXCoordinate());
////                System.out.println("Coordinates: " + ball.getXCoordinate() + ":" + ball.getYCoordinate());
////                System.out.println("ball.getAngle() = " + ball.getAngle());
//            } else {
//                ball.setXCoordinate(paddle.getXCoordinate() - ball.getDiameter());
////                System.out.println("x should be at most: " + paddle.getXCoordinate());
////                System.out.println("Coordinates: " + ball.getXCoordinate() + ":" + ball.getYCoordinate());
////                System.out.println("ball.getAngle() = " + ball.getAngle());
//            }

        // Move ball outside of paddle to prevent problems

//            return true;
//        }
//        return false;
    }

//    private void resetBall(AbstractPlayer playerThatLost, Ball ballThatScored) {
////        System.out.println("playerThatLost.getPaddle().getXCoordinate() = " + playerThatLost.getPaddle().getXCoordinate());
////        mGameState = 0;
////        mRoundStartTime = System.currentTimeMillis();
//        // The ball shoots towards the center of the player that lost
//        Paddle paddle = playerThatLost.getPaddle();
//        float xDif = ((paddle.getCornerLeftTop().getX() - paddle.getCornerRightBottom().getX()) / 2 + paddle
//                .getCornerRightBottom().getX()) - (loadScreenWidth() / 2);
//        float yDif = ((paddle.getCornerLeftTop().getY() - paddle.getCornerRightBottom().getY()) / 2 + paddle
//                .getCornerRightBottom().getY()) - (loadScreenHeight() / 2);
//        int angle = (int) ((Math.toDegrees(Math.atan2(yDif, xDif)) + 90) % 360);
//        ballThatScored.resetBall(loadScreenWidth() / 2 - loadBallDiameter(), loadScreenHeight() / 2 - loadBallDiameter(), angle);
//        // TODO: continue here
//
//    }


    public JFrame getFrame() {
        return mFrame;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (mPressedKeys.indexOf(e.getKeyCode()) != -1) {
            // Old press
            return;
        }
        // New press
        int keyCode = e.getKeyCode();
        mPressedKeys.add(keyCode);
        switch (keyCode) {

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        int position = mPressedKeys.indexOf(e.getKeyCode());
        if (position != -1) {
            mPressedKeys.remove(position);
        }
    }
}
